from weightcalculation.testgraphs import read_graph


def sum_of_neighboor(graph, vertex):
    """
    Cumpute summury value of assets of all vertecies which neighbor to vertex
    :param graph: igraph.Graph
    :param vertex: vertex id
    :param mode: "in" - for assets "out" - for lianilities
    :return: sum of vertex neigbors
    """
    arr = graph.neighborhood(vertex, mode="out")[1:]
    if arr:
        return sum(map(lambda x: graph.vs["assets"][x], arr))
    else:
        return 0


def add_weight(graph, edge):
    """
    Compute edge's weight
    :param graph:
    :param edge: graph edge, represents as a tuple
    :return:
    """
    out, to = edge
    assets = graph.vs["assets"][to]
    liabilities = graph.vs["liabilities"][out]
    summ = sum_of_neighboor(graph, out)
    if summ != 0:
        graph[out, to] = liabilities/summ*assets
    else:
        # week place, what if liabilities > assets
        sum2 = sum(map(lambda x: graph[x, to] if graph[x, to] else 0, graph.neighborhood(to, mode="in")))
        graph[out, to] = assets - sum2


def update_weight(graph, vertex):
    """
    Update all edge's weight which are neighbor of vertex
    :param graph: graph to mutate
    :param vertex: index vertex
    :return:
    """
    for i in graph.neighborhood(vertex, mode="out")[1:]:
        edge = (vertex, i)
        add_weight(graph, edge)


def add_edge_with_weight(graph, edge):
    """
    :param graph: graph to mutate
    :param edge: tuple of vertex index
    :return:
    """
    start, end = edge
    graph.add_edge(start, end, weight=0)
    update_weight(graph, start)


def delete_edge_with_weight(graph, sample):
    e_lst = graph.get_edgelist()
    # print "maxSmaple: {0}\tecount: {1}\tlenElst: {2} ".format(max(sample),graph.ecount(), len(e_lst))
    vertex_to_update = list(map(lambda x: e_lst[x-1] if x != 0 else e_lst[x], sample))
    graph.delete_edges(sample)
    for i in vertex_to_update:
        start, end = i
        update_weight(graph, start)


def delete_edge_after_add(graph, edge):
    e_lst = graph.get_edgelist()
    graph.delete_edges(edge)
    if edge in e_lst:
        start, end = edge
        update_weight(graph, start)


if __name__ == '__main__':
    g = read_graph("test_10.graphml")
    print [(0,1), (0,2), (0,3)].pop(0).index(0)