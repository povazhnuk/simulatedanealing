from weightcalculation.testgraphs import read_graph


def get_received_sum(graph, vertex, mode):
    arr = graph.neighborhood(vertex, mode=mode)[1:]
    if mode == "in":
        return sum(map(lambda x: graph[x, vertex] if graph[x, vertex] else 0, arr))
    if mode == "out":
        return sum(map(lambda x: graph[x, vertex] if graph[x, vertex] else 0, arr))


def get_diff(graph, a_l):
    """
    Used to check correctness of algorithm
    :param graph: igraph.Graph
    :param a_l: "assets" or "liabilities"
    :return: sum of difference between true and received a_l
    """
    if a_l == "assets":
        mode = "in"
    elif a_l == "liabilities":
        mode = "out"
    else:
        raise ValueError("the second parameter must be 'assets' or 'liabilities'")

    return sum(map(lambda x: graph.vs[a_l][x] - get_received_sum(graph, x, mode), range(0, graph.vcount())))


if __name__ == '__main__':
    g = read_graph("test_10.graphml")
